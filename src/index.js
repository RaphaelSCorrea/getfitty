import React from 'react';
import ReactDOM from 'react-dom';
import Chat from './pages/chat';

ReactDOM.render(<Chat />, document.getElementById('root'));
