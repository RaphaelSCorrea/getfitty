import React from 'react';

import { Nav, NavButton, HeaderContainer } from './style';

import { ReactComponent as Home } from '../../icons/home.svg';
import { ReactComponent as Treino } from '../../icons/treino.svg';
import { ReactComponent as Nutri } from '../../icons/nutri.svg';
import { ReactComponent as Aviso } from '../../icons/aviso.svg';
import { ReactComponent as Perfil } from '../../icons/user.svg';
import { ReactComponent as Config } from '../../icons/config.svg';

const Navigation = () => {
  return(
    <Nav>
      <HeaderContainer>

        <NavButton>
          <Home />
          <span> HOME </span>
        </NavButton>

        <NavButton>
          <Treino />
          <span> Treino </span>
        </NavButton>

        <NavButton>
          <Nutri />
          <span> Nutri </span>
        </NavButton>

        <NavButton>
          <Aviso />
          <span> Aviso </span>
        </NavButton>

        <NavButton>
          <Perfil />
          <span> Perfil </span>
        </NavButton>

        <NavButton>
          <Config />
          <span> Config </span>
        </NavButton>
      </HeaderContainer>
    </Nav>
  );
}

export default Navigation;