import styled from 'styled-components';

export const Header = styled.header`
  height: 85px;
  background: linear-gradient(180.78deg, #7F65E5 -8.68%, #434489 96.86%);
  position: sticky;
  top: 0;
  left: 0;
`;

export const HeaderTitle = styled.h3`
  font-weight: 600;
  font-size: 20px;
  line-height: 20px;
  color: #fff; 
`;

export const HeaderSubTitle = styled.h4`
  font-weight: normal;
  font-size: 15px;
  line-height: 20px;
  color: #fff;
`;


export const Container = styled.div`
  width: 100%;
  padding: 0 16px;
`;

export const HeaderContainer = styled(Container)`
  height: 100%;
  display: flex;
  flex: 1;
  justify-content: space-between;
  align-items: center;
`;

export const Avatar = styled.div`
  border-radius: 50%;
  border: solid 2px  #FFD200;
  width: 56px;
  height: 56px;
  box-shadow: 0px 2px 30px #626262;
  margin-right: 15px;
  background-image: url('${ props => props.img ? props.img : ''}');
`;

export const Nav = styled.nav`
  width: 100%;
  height: 90px;
  
  left: 0;
  bottom: 0;

  background: linear-gradient(180.78deg, #7F65E5 -8.68%, #434489 96.86%);

  position: sticky;
  bottom: 0;
  left: 0;
`;

export const NavButton = styled.a`
  text-align: center;
  span {
    display: block;
    letter-spacing: 0.171429px;
    font-weight: 500;
    font-size: 12px;
    line-height: 14px;
    font-style: normal;
    color: #fff;
    text-transform: uppercase;
    margin-top: 5px;
  }
`;