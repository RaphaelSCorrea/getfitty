import styled from 'styled-components';

export const ListItem = styled.li`
  padding: 18px 25px;
  background: ;
  background: ${props => props.isRead ? '#fff' : '#efe6f8'};
  border-bottom: 1px solid #ccd1d6;
  display: flex;
  transition: all .5 linear;
`;

export const Avatar = styled.div`
  border-radius: 10px;
  width: 56px;
  height: 56px;
  margin-right: 10px;
  background-blend-mode: overlay, normal;
  background-image: url('${ props => props.img ? props.img : ''}');
`;

export const Title = styled.h3`
  letter-spacing: 0.2px;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 17px;
  color: #131315;
  margin-left: 10px;
`;

export const Pill = styled.div`
  max-height: 14px;
  margin-left: 40px;
  padding: 2px 5px;
  border-radius: 4px;
  color: #fff;
  font-size: 9px;
  text-align: center;
  letter-spacing: 0.171429px;
  text-transform: uppercase;

  background: ${props => props.isRead ? '#DAE1E9' : ' #6155CC'};
`;

export const Prev = styled.div`
  width: 100%;

  p {
    font-weight: normal;
    font-size: 14px;
    line-height: 17px;
    color: #2D3142;
  }

  span {
    color: #6155CC;
  }
`;