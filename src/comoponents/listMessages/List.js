import React, { useRef, useState, useEffect } from 'react';

import { 
  ListItem,
  Avatar,
  Title,
  Pill,
  Prev
} from './style';

import { ReactComponent as Phone } from '../../icons/phonelink_ring_24px.svg'


const List = (props) => {
  const listElement = useRef(); 
  const { PreveMessage, avatar, fromName, date, isRead } = props;

  const [ dragged, setDragged ] = useState(false);
  const [ dragStartX, setDragStartX] = useState(0);
  const [ left , setLeft ] = useState(0);
  const [fpsInterval, setFpsInterval] = useState(60);
  const [startTime , setStartTime] = useState(null);

  const onDragStartTouch = (evt) => {
    const touch = evt.targetTouches[0];
    setStartTime(Date.now());
    onDragStart(touch.clientX);
    window.addEventListener("touchmove", onTouchMove);
  };

  useEffect(()=>{
    window.addEventListener("touchend", onDragEndTouch);
  });

  useEffect(()=>{
    window.removeEventListener("touchend", onDragEndTouch);
  },[]);

  const onDragStart = (clientX) => {
    setDragged(true);
    setDragStartX(clientX);
    requestAnimationFrame(updatePosition);
  };

  const onTouchMove = (evt) => {
    const touch = evt.targetTouches[0];
    setDragStartX(touch.clientX - dragStartX);
    setDragged(true);

    if (left < touch.clientX) {
      setLeft(touch.clientX);
      console.log(left,'entrei')
    }
  };


  const onDragEndTouch = () => {
    window.removeEventListener("touchmove", onTouchMove);
    onDragEnd();
  };

  const onDragEnd = () => {
    if (dragged) {
      setDragged(false);

      // if ( left > 0) {
      //   setLeft(0);
      // } 
    }
  };

  const updatePosition = () => {
    if (dragged) window.requestAnimationFrame(updatePosition);
    
    const now = Date.now();
    const elapsed = now - startTime;
    
    if (dragged && elapsed > fpsInterval) {
      listElement.current.style.transform = `translateX(${left}px)`;
      setStartTime(Date.now());
    }
  };

  return(
    <ListItem
      ref={listElement}
      isRead={isRead}
      onTouchStart={onDragStartTouch}
      onTouchMove={touchMoveEvent => onTouchMove(touchMoveEvent)}
      onTouchEnd={onDragEnd}
    >
      <div> 
        <Avatar 
          img={avatar}
        />
      </div>
      
      <div style={{display: 'flex', flexWrap: 'wrap'}}>
        <div style={{display: 'flex', alignItems: 'center', width: '100%', marginBottom: '6px'}}> 
          <Phone /> 
          <Title> {fromName} </Title> 
          <Pill isRead={isRead}> {date} </Pill>
        </div>

        <Prev 
          dangerouslySetInnerHTML={{ __html: PreveMessage }}
        /> 
      </div>
    </ListItem>
  );
};

export default List;
