import React, { useState, useEffect } from 'react';
import { getMessages } from '../services/messeges';

import { 
    Header,
    HeaderContainer,
    HeaderTitle,
    HeaderSubTitle,
    Avatar
} from '../comoponents/header/style';

import Nav from '../comoponents/header/Nav';
import List from '../comoponents/listMessages/List';


import { ReactComponent as MessageIcon } from '../icons/close-envelope.svg';

const RenderList = (messages) => {
  return messages.map((item, i)=> {
    return (
      <List 
        key={item.id}
        avatar={item.avatar}
        fromName={item.fromName}
        date={item.date}
        PreveMessage={item.prev}
        isRead={item.isRead}
      />
    )
  });
}

const RenderLoad = () => {
  return (<p> Carregando ... </p>);
}

const Chat = () => {

  const [ messages, setMessages ] = useState(null);
  
  useEffect(() => {
    const loadMessages = async () => { 
      setMessages(await getMessages());
    }
    loadMessages();
  } , []);
    
  return (
    <>
      <Header>
        <HeaderContainer>
          <div style={{display: 'flex', alignItems: 'center'}}>
            <Avatar 
              img="/images/avatar.png"
            />
            <div>
              <HeaderTitle> Bom dia Heitor </HeaderTitle>
              <HeaderSubTitle> Treino do dia pendente </HeaderSubTitle>
            </div>
          </div>
          <MessageIcon />
        </HeaderContainer>
      </Header>

      { messages ? RenderList(messages.listMessages) : RenderLoad() }


      <Nav /> 
    </>
  );
}

export default Chat;
