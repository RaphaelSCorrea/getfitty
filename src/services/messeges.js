import axios from 'axios';

const BASE_URL = '';

export const getMessages = async () => {
  try {
    const URL = `${BASE_URL}/messages`;
    const resp = await axios.get(URL);
    return resp.data;
  } catch(e) {
    console.log(e)
  }
}
